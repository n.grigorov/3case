from abc import ABC
import pathlib
import jinja2
import pandas as pd

class FileProcessor(ABC):  # Это абстрактный класс
    def read_file(self, file_name):
        pass

    @staticmethod
    def select_data(df, col_name, value):
        #filtered_df =
        return df[df[col_name] == value]

    @staticmethod
    def save_data(df, city_name):
        tmplt_file_name = pathlib.Path.cwd() / 'tmplt' / 'table.html'
        with open(tmplt_file_name) as file:
            tmplt_str = file.read()
            row_title = ('DAY', 'TOWN', 'WEATHER')
            html_file_name = city_name + '.html'
            html_file_name = pathlib.Path.cwd() / html_file_name
            with open(html_file_name, 'w+') as html_file:
                html_file.write(jinja2.Template(tmplt_str).render(the_row_titles=row_title, the_data=df.values))

    @staticmethod
    def file_check(file_name):
        if pathlib.Path(file_name).exists():
            return True
        else:
            return False

    @staticmethod
    def add_quotes(df, col_name):
        df.loc[df[col_name] != '', col_name] = '"' + df[col_name] + '"'


class CsvProcessor(FileProcessor):

    def __init__(self, file_name):
        self.file_name = pathlib.Path.cwd() / file_name
    def read_file(self):

        if self.file_check(self.file_name):
            df = pd.read_csv(self.file_name, index_col=False)
            return df
        else:
            raise Exception('Не существет файла: ' + self.file_name.name)


class XlsxProcessor(FileProcessor):
    def __init__(self, file_name):
        self.file_name = pathlib.Path.cwd() / file_name
    def read_file(self):
        if self.file_check(self.file_name):
            df = pd.read_excel(self.file_name, sheet_name='Worksheet')
            return df
        else:
            raise Exception('Не существет файла: ' + self.file_name.name)


class JsonProcessor(FileProcessor):
    def __init__(self, file_name):
        self.file_name = pathlib.Path.cwd() / file_name
    def read_file(self):
        if self.file_check(self.file_name.name):
            df = pd.read_json(self.file_name.name)
            return df
        else:
            raise Exception('Не существет файла: ' + self.file_name.name.name)


def process_file(file_name, col_key, col_value):
    ext = file_name[file_name.rfind(".")+1:]
    if ext == 'json':
        json = JsonProcessor(file_name)
        data_fr = json.read_file()
        filtered_df = json.select_data(data_fr, col_key, col_value)
        #json.add_quotes(filtered_df, col_key)
        json.save_data(filtered_df, col_value)

    elif ext == 'csv':
        csv = CsvProcessor(file_name)
        data_fr = csv.read_file()
        filtered_df = csv.select_data(data_fr, col_key, col_value)
        csv.save_data(filtered_df, col_value)

    elif ext == 'xlsx':
        xlsx = XlsxProcessor(file_name)
        data_fr = xlsx.read_file()

        filtered_df = xlsx.select_data(data_fr, col_key, col_value)

        xlsx.save_data(filtered_df, col_value)


#input_name = 'json_data.json'
process_file('json_data.json', 'town', 'Tambov')

process_file('data_xls.xlsx', 'town', 'Moscow')

process_file('data.csv', 'town', 'Lipetsk')

#проверить наличие данных в файле???



